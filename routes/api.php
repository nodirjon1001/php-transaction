<?php

use App\Http\Controllers\BankController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\GetCreditController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('indexBank',[BankController::class,'index']);
Route::post('createBank',[BankController::class,'create']);
Route::delete('deleteBank/{id}',[BankController::class,'delete']);

Route::get('indexUser',[UserController::class,'index']);
Route::post('createUser',[UserController::class,'create']);
Route::put('updateUser/{id}',[UserController::class,'update']);
Route::delete('deleteUser/{id}',[UserController::class,'logOut']);

Route::get('indexCard',[CardController::class,'index']);
Route::post('createCard',[CardController::class,'create']);
Route::put('subtraction/{id}',[CardController::class,'subtraction']);
Route::put('addition/{id}',[CardController::class,'addition']);
Route::delete('deleteCard/{id}',[CardController::class,'delete']);

Route::get('indexCredit',[GetCreditController::class,'index']);
Route::post('getCredit',[GetCreditController::class,'getCredit']);
// Route::get('indexCredit',[GetCreditController::class,'index']);