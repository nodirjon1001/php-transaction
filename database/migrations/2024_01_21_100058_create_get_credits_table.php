<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('get_credits', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('bank_id');
            $table->bigInteger('card_id');
            $table->bigInteger('user_id');
            $table->bigInteger('totalCredit');
            $table->integer('date');

            $table->foreign('bank_id')->references('bank')->on('id')->onDelete('cascade');
            $table->foreign('user_id')->references('user')->on('id')->onDelete('cascade');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ge_credits');
    }
};
