<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('type');
            $table->bigInteger('number')->unique();
            $table->bigInteger('amount')->default(0);
            $table->bigInteger('bank_id');
            $table->bigInteger('user_id');
            $table->foreign('user_id')->references('user')->on('id')->onDelete('cascade');
            $table->foreign('bank_id')->references('bank')->on('id')->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cards');
    }
};
