<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class getCredit extends Model
{
    use HasFactory;
    protected $fillable =['bank_id','card_id','user_id','totalCredit','date'];

    public function getCredit(){
        return $this->belongsTo(Bank::class);
    }
}
