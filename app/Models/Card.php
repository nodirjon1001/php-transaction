<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;
    protected $fillable = ['name','type','number','amount','bank_id','user_id'];

    public function users(){
        return $this->belongsTo(User::class,'id');
    }
}
