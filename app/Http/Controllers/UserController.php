<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(){
       return $user = User::with(['cards'])->get();
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required',
        ]);
        if(!$validator->fails()){
            $user = User::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>$request->password,
            ]);
            return response()->json([
                $user->name.' succsessful addition'
            ]);
        }else{
            return response()->json(['Error addition']);
        }
    }

    public function update(Request $request, $id){
        $user = User::findOrFail($id);
        if($user){
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $user->update();
            $user->save();
        }
    }
    public function logOut(Request $request, $id){
        $user = User::findOrFail($id);
        if($user){
            $user->delete();
            return response()->json([$user->name. ' delete']);
        }else{
            return response()->json(['error']);
        }
    }

    
}
