<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Card;
use App\Models\getCredit;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GetCreditController extends Controller
{
    public function index(){
     return $credit = getCredit::all();
    }

public function getCredit(Request $request){
    $request->validate([
        'bank_id' => 'required',
        'user_id' => 'required',
        'card_id' => 'required',
        'totalCredit' => 'required',
        'date' => 'required',
    ]);

    $bank = Bank::findOrFail($request->bank_id);
    $user = User::findOrFail($request->user_id);
    $card = Card::findOrFail($request->card_id);

    $activeCredit = GetCredit::where('card_id', $card->id)->whereNull('totalCredit')->first();
    if ($activeCredit) {
        return response()->json(['message' => 'Sizda hali qaytarilmagan kredit bor']);
    }
    if ($card->amount !== null && $bank->balance >= $request->totalCredit){
        $bank->decrement('balance', $request->totalCredit);   
        $card->increment('amount', $request->totalCredit);
    } else {
        return response()->json(['message' => "Kartada yetarli mablag' yo'q yoki karta mavjud emas"]);
    }
   
    $percentage = ceil ($request->totalCredit * 0.26 /12);
    $remainingMonths = ceil($request->totalCredit / $request->date + $percentage );
    $qolganOylar = $request->date;

    for ($i = 1; $i <= $request->date; $i++) {
        GetCredit::create([
            'bank_id' => $request->bank_id,
            'user_id' => $request->user_id,
            'card_id' => $request->card_id,
            'totalCredit' => $remainingMonths,
            'date' => $i,
        ]);
    }

    return response()->json([
        'message' => 'Sizga ' . $request->totalCredit . ' miqdorda Credit ajratildi',
        'info' => 'Siz bu creditni har oy ' . $remainingMonths . ' so\'m to\'lashingiz shart',
        'oylik' => $qolganOylar . " oy davomida to'lash muddati"
    ]);
}
}
