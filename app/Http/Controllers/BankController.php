<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BankController extends Controller
{
    public function index(){
       return $bank = Bank::all();
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'balance'=>'required',
        ]);
        if(!$validator->fails()){
            $bank = Bank::create([
                'name'=>$request->name,
                'balance'=>$request->balance,
            ]);
            return response()->json([
                'message'=> $bank->name.' create'
            ]);
        }else{
            return null;
        }
    }

    public function delete(Request $request,$id){
        $bank = Bank::findOrFail($id);
        if($bank){
            $bank->delete();
            return response()->json(['message'=>$bank->name. ' delete']);
        }
    }
}
