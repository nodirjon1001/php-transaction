<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CardController extends Controller
{
    public function index(){
        return $card = Card::with(['users'])->get();
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',   
            'bank_id' => 'required',
            'amount' => 'required',

        ]);
    
        if ($validator->fails()) {
            return $validator->errors();
        } else {
            $type = $request->type;
            $humo = '9860';
            $uzcard = '8600';
    
            $result = '';
            for ($i = 0; $i < 12; $i++) {
                $result .= random_int(0, 9);
            }
    
            $cardNumber = ($type === 'humo') ? $humo . $result : $uzcard . $result;
    
            $card = Card::create([
                'name' => $request->name,
                'user_id' => $request->user_id,
                'bank_id' => $request->bank_id,
                'number' => $cardNumber,
                'type' => $type,
                'amount' => $request->amount
            ]);
            return response()->json([
                'message'=> 'Successfully create',
                'type'=>$type,
                'number'=>$cardNumber
            ]);
        }
    }
    public function subtraction(Request $request, $id){
        $card = Card::findOrFail($id);
        if($card){
            $card ->amount -= $request->amount;
            $card->update();
            return response()->json([
                'money withdrawn'=>$request->amount,
                'the rest of the money'=>$card->amount,
            ]);
        }else{
            return response()->json(['There is not enough money on the Card']);
        }
    }

    public function addition(Request $request, $id){
        $card = Card::findorFail($id);
        if($card){
            $card->amount += $request->amount;
            $card->update();
            return response()->json([
                'money added to the card'=>$request->amount,
                'total balance'=>$card->amount,
            ]);
        }
    }

    public function delete(Request $request, $id){
        $card = Card::findorFail($id);
        if($card){
            $card->delete();
            return response()->json([$card->name && $card->type . 'delete']);
        }else{
            return response()->json(['error delete']);
        }
    }
}
